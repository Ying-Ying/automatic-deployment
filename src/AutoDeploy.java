import java.util.concurrent.TimeUnit;
import java.net.URLEncoder;
import java.util.Scanner;
import chttl.cloud.hws.HWSSignature;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.text.SimpleDateFormat;

public class AutoDeploy {
        /**
         * Generate the example signature from your secret key and URL format string, then print it out on Console application
         * @param String[] arg
         * @return String signature
         */
    public static String hwsRestPrefix="https://hws.hicloud.hinet.net/cloud_hws/api/hws/?";
    public static String secretkey="";////change it to your secretkey
    public static String accessKey="";////change it to your accesskey
    public static String expiredtime="";
    //print descirbe table
    public static void printout(String content){
        JSONObject json = JSONObject.fromObject(content);
        JSONArray instanceList =JSONArray.fromObject(json.getString("instanceList"));
        System.out.printf("%-5s %-30s %-20s %13s\n","Index","Name","Operation Status","Instance Id");
        for(int i=0;i<instanceList.size();i++){
           String index = Integer.toString(i+1);
           String Name = JSONObject.fromObject(instanceList.get(i)).getString("name");
           String opS = JSONObject.fromObject(instanceList.get(i)).getString("operationStatus");
           String Id = JSONObject.fromObject(instanceList.get(i)).getString("instanceId");
           System.out.printf("%-5s %-30s %-20s %13s\n",index,Name,opS,Id);
        }
    }
    //print provision status
    public static String printprostatus(String content){
        JSONObject json = JSONObject.fromObject(content);
        JSONArray instanceList =JSONArray.fromObject(json.getString("instanceList"));
        String provisionStatus="";
        String pros = "";
        System.out.printf("%-5s %-30s %-20s %13s\n","Index","Name","Operation Status","Instance Id","Provision status");
        for(int i=0;i<instanceList.size();i++){
           String index = Integer.toString(i+1);
           String Name = JSONObject.fromObject(instanceList.get(i)).getString("name");
           String opS = JSONObject.fromObject(instanceList.get(i)).getString("operationStatus");
           String Id = JSONObject.fromObject(instanceList.get(i)).getString("instanceId");
           pros = JSONObject.fromObject(instanceList.get(i)).getString("provisionStatus");
           System.out.printf("%-5s %-30s %-20s %13s %30s\n",index,Name,opS,Id,pros);
        }
        return pros;
    }
    //run restful api
    public static String run(String result){
            try (java.util.Scanner s = new java.util.Scanner(new java.net.URL(result).openStream(), "UTF-8").useDelimiter("\\A")) {
                return s.next();
        } catch (java.io.IOException e) {
                e.printStackTrace();
                return "";
        }
     }
    //function for listing all instances' info
    public static void describeALLInstance(){
        String instruction = HWSSignature.getSignedUrl(secretkey,hwsRestPrefix+"action=describeInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime);
        String content = run(instruction);
        printout(content);
    }
    //function for listing specific instances' info
    public static void describeInstance(String[] args){
        String instruction = hwsRestPrefix+"action=describeInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime;
        for(int i = 1; i < args.length ; i++) {
            instruction+="&instanceId="+args[i];
        }
        String result = HWSSignature.getSignedUrl(secretkey,instruction);
        String content = run(result);
        printout(content);
    }
    //check provision status
    public static void checkprostatus(String args){
        String provisionStatus="";
        while(!provisionStatus.equals("provisionok")){
            
            String instruction = hwsRestPrefix+"action=describeInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime+"&instanceId="+args;
            String result = HWSSignature.getSignedUrl(secretkey,instruction);
            String content = run(result);
            provisionStatus = printprostatus(content);
            if(!provisionStatus.equals("provisionok")){
                System.out.println("The target VM is cloning for other VM. Please wait.");
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace(); 
                }
//                TimeUnit.SECONDS.sleep(1); 
            }
        }
        
    }
    public static void generateInstance(String[] args){
        //clone VM by VM
        String instanceId="";
        String name = "";
        int numberbig = 0;
        int number = 0;
        try {
            int i = Integer.parseInt(args[1]);
            System.out.println(i);
            while(i>0){
            	if(i>120){
                	//type 3: 4vCPU 32RAM
                	instanceId="BV55018998001Y";
                	numberbig=i/120;
                	i=i%120;
                	System.out.println(number);
            	}
            	else if(i>60){
                	//type 3: 4vCPU 32RAM
                	instanceId="BV55018998001Y";
                	number = 1;
                	i = 0;
            	}
            	else if(i>30){
                	//type 2: 4vCPU 16RAM
                	instanceId="BV55018998001X";
                	number = 1;
                        i = 0;
            	}
            	else{
                	//type 1: 4vCPU 8RAM
                	instanceId="BV55018998001W";
                	number = 1;
                        i = 0;
            	}
            }
        } catch (NumberFormatException e) {}
        int all = number + numberbig;
        System.out.println("According to the class members, we will generate "+all+" VMs.");
        if(number>0){
	//generate one VM with corresponding instanceId
    	    checkprostatus(instanceId);
    	    String instruction = hwsRestPrefix+"action=runInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey+"&monitoringEnabled=false"+ "&instanceId="+instanceId + "&count=1"+ "&expires="+expiredtime;
    	    Scanner scan = new Scanner(System.in);
    	    System.out.println("Enter the name for VM :");
    	// 判断是否还有输入
    	    if (scan.hasNext()) {
    	        name = scan.next();
    	        System.out.println("The name of VM is " + name);
    	    }
    	    instruction+="&instanceName="+name;
    	    System.out.println(instruction);
    	    String result = HWSSignature.getSignedUrl(secretkey,instruction);
    	    String content = run(result);
    	    System.out.println(content);
	}
        if(numberbig>0){
	    //generate type 3 VMs
            instanceId="BV55018998001Y";
	    for(int i=0;i<numberbig;i++){
	        checkprostatus(instanceId);
	        String instruction = hwsRestPrefix+"action=runInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey+"&monitoringEnabled=false"+ "&instanceId="+instanceId + "&count=1"+ "&expires="+expiredtime;
	        Scanner scan = new Scanner(System.in);
	        System.out.println("Enter the name for VM :");
	        // 判断是否还有输入
	        if (scan.hasNext()) {
	            name = scan.next();
	            System.out.println("The name of VM is " + name);
	        }
	        instruction+="&instanceName="+name;
	    System.out.println(instruction);
	    String result = HWSSignature.getSignedUrl(secretkey,instruction);
	    String content = run(result);
	    System.out.println(content);
	    }
        }
    }

    public static void startInstance(String[] args){
        String instruction = hwsRestPrefix+"action=startInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime;
        for(int i = 1; i < args.length ; i++) {
            instruction+="&instanceId="+args[i];
        }
        String result = HWSSignature.getSignedUrl(secretkey,instruction);
        String content = run(result);
        System.out.println(content);
    }
    public static void stopInstance(String[] args){
        String instruction = hwsRestPrefix+"action=stopInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime;
        for(int i = 1; i < args.length ; i++) {
            instruction+="&instanceId="+args[i];
        }
        String result = HWSSignature.getSignedUrl(secretkey,instruction);
        String content = run(result);
        System.out.println(content);
    }
    public static void rebootInstance(String[] args){
        String instruction = hwsRestPrefix+"action=rebootInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime;
        for(int i = 1; i < args.length ; i++) {
            instruction+="&instanceId="+args[i];
        }
        String result = HWSSignature.getSignedUrl(secretkey,instruction);
        String content = run(result);
        System.out.println(content);
    }
    public static void terminateInstance(String[] args){
        String instruction = hwsRestPrefix+"action=terminateInstances"+ "&version=2018-12-12&chtAuthType=hwspass"+ "&accessKey="+accessKey
                                        + "&expires="+expiredtime;
        for(int i = 1; i < args.length ; i++) {
            instruction+="&instanceId="+args[i];
        }
        String result = HWSSignature.getSignedUrl(secretkey,instruction);
        String content = run(result);
        System.out.println(content);
    }
        public static void main(String[] args) throws ParseException {
        Long now_time = System.currentTimeMillis();
        int minute = 5;//change to set expired time for instructions
        Long after_time = now_time + minute*60*1000;
        expiredtime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(after_time).toString();
        //***Definition Stage***
        // create Options object
        Options options = new Options();
        // add option "-l"
        options.addOption("l", false, "List informations of VMs. It will list all machines if no arguments given. Arguments: VM's instanceId. Usage: -l BV55018998000A BV55018998000B");
        // add option "-g"
        options.addOption("g", true, "Generate new VMs and auto deploy EduTalk v2. Usage example for generating a machine for 30 class members: -g 30");
        // add option "-u"
        options.addOption("u", true, "Startup specific VMs. Usage: -u BV55018998000A BV55018998000B");
        // add option "-d"
        options.addOption("d", true, "Shutdown specific VMs. Usage: -d BV55018998000A BV55018998000B");
        // add option "-r"
        options.addOption("r", true, "Restart specific VMs.  Usage: -r BV55018998000A BV55018998000B");
        // add option "-c"
        options.addOption("c", true, "Leaseback specific VMs if no longer useful.  Usage: -c BV55018998000A BV55018998000B");
        // add option "-t"
        //options.addOption("t", true, "Set expired time for instructions. Unit: minute.  Usage: -t 10");
        //***Parsing Stage***
        //Create a parser
        CommandLineParser parser = new DefaultParser();
        //parse the options passed as command line arguments
        CommandLine cmd = parser.parse( options, args);
        //***Interrogation Stage***
        //hasOptions checks if option is present or not
        if(cmd.hasOption("l")) {
            if(args.length==1){
                describeALLInstance();
                System.out.println("Already list all info for machines.");
            }
            else{
                describeInstance(args);
                System.out.println("Already list the following machine's info:");
                for(int i = 1; i < args.length ; i++) {
                   System.out.println(args[i]);
                 }
            }
        } else if(cmd.hasOption("g")) {
            generateInstance(args);
           System.out.println("Generating new machine.");
        } else if(cmd.hasOption("u")) {
            startInstance(args);
           System.out.println("Turning on machine.");
        } else if(cmd.hasOption("d")) {
            stopInstance(args);
           System.out.println("Turning off machine.");
        } else if(cmd.hasOption("r")) {
            rebootInstance(args);
           System.out.println("Restarting machine.");
        } else if(cmd.hasOption("c")){
            terminateInstance(args);
           System.out.println("Leaseback machine.");
        }
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("AutoDeploy", options);
}
}
